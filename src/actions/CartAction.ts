import { getAccessToken } from "../components/utils/accessToken";
import { changePort } from "./AuthActions";

export const checkout = async (itemId: [number?], quantity: [number?], dispatch: any) => {
    dispatch({ type: 'CHECKOUT' });
    try {
        const token = getAccessToken();
        const res = await fetch(`http://localhost:${changePort}/item/checkout`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token ? `Bearer ${token}` : ''
            },
            body: JSON.stringify({
                itemId,
                quantity
            })
        });
        dispatch({ type: 'CHECKOUT_FULFILLED', payload: res });
    } catch (error) {
        dispatch({ type: 'CHECKOUT_FULFILLED', payload: error });
    }
}