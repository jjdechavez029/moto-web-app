import { Items } from "../contexts/dashboard/ItemContext";

export type Cart = {
    id: number;
    name: string;
    quantity: number;
    totalAmount: number;
}

type iStatus = {
    sending: boolean;
    sent: boolean;
    error: string | null;
}

export interface CartState {
    cart: Cart[],
    checkoutStatus: iStatus
}

export const CartReducer = (
    state: CartState,
    { type, payload } : { type: string, payload: any | null }
) => {
    switch(type) {
        case 'INSERT_INTO_CART': {
            const { id, items } = payload;
            let container: Cart[] = [];

            items.filter((itemFilter: Items) => {
                if (itemFilter.id === id) {
                    let obj = {
                        id: itemFilter.id!, 
                        name: itemFilter.name,
                        quantity: 1,
                        totalAmount: itemFilter.price
                    }
                    container.push(obj);
                }
            });

            let cart = [...state.cart, ...container];

            return { ...state, cart } 
        }
        case 'REMOVE_INTO_CART': {
            const { id } = payload
            let cart = state.cart.filter((item: Cart) => item.id !== id);
            console.log('remove from cart', cart)

            return { ...state, cart } 
        }
        case 'CLEAR_CART': {
            return { ...state, cart: [] }
        }
        case 'INCREASE_CART_ITEM_QUANTITY': {
            let updateItemQuantity = state.cart.map((item: Cart) => {
                if (item.id === payload) {
                    item.quantity += 1;
                    return item
                }
                return item;
            });
            console.log('inc cart itemquantity', updateItemQuantity)

            return { ...state, cart: updateItemQuantity }
        }
        case 'DECREASE_CART_ITEM_QUANTITY': {
            let updateItemQuantity = state.cart.map((item: Cart) => {
                if (item.id === payload) {
                    item.quantity -= 1;
                    return item
                }
                return item;
            });

            console.log('dec cart itemquantity', updateItemQuantity)

            return { ...state, cart: updateItemQuantity }
        }
        case 'CHECKOUT': {
            const status = {
                ...state.checkoutStatus,
                sending: true
            };
            return {  ...state, checkoutStatus: status }
        }
        case 'CHECKOUT_FULFILLED': {
            const status = {
                ...state.checkoutStatus,
                sending: false,
                sent: true
            };
            return {
                ...state,
                checkoutStatus: status,
                cart: []
            }
        }
        case 'CHECKOUT_FAILED': {
            const status = {
                ...state.checkoutStatus,
                sent: false,
                error: payload
            }
            return { ...state, checkoutStatus: status }
        }
        default:
            return state;
    }
}