import React, { createContext, useReducer, useState } from "react";
import { CartReducer, CartState } from "../../reducers/CartReducer";
import logger from "use-reducer-logger";

interface CartContext {
    state: {
        checkout: boolean;
        finishedCheckout: boolean;
    };
    setState: {
        setCheckout: React.Dispatch<React.SetStateAction<boolean>>;
        setFinishedCheckout: React.Dispatch<React.SetStateAction<boolean>>;
    };
    cartState: CartState;
    dispatch({ type, payload } : { type: string, payload?: any | null }): void;
}

export const CartContext = createContext({} as CartContext);

const CartContextProvider = (props: any) => {
    const [checkout, setCheckout] = useState(false);
    const [finishedCheckout, setFinishedCheckout] = useState<boolean>(false);
    const initialState = {
        cart: [],
        checkoutStatus: {
            sending: false,
            sent: false,
            error: null
        }
    }

    // const [cartState, dispatch] = useReducer(
    //     process.env.NODE_ENV === 'development' 
    //         ? logger(CartReducer) 
    //         : CartReducer, initialState
    // );
    const [cartState, dispatch] = useReducer(CartReducer, initialState);

    const value = {
        state: {
            checkout,
            finishedCheckout
        },
        setState: {
            setCheckout,
            setFinishedCheckout
        },
        cartState,
        dispatch
    }
    return (
        <CartContext.Provider value={value}>
            {props.children}
        </CartContext.Provider>
    )
}

export default CartContextProvider;