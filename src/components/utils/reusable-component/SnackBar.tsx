import React, { useEffect, memo } from "react";
import { Snackbar } from "@material-ui/core";
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props: any) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function DisplaySnackbar(
  { isOpen }: { isOpen: boolean }
) {
  const [open, setOpen] = React.useState<boolean>(false);

  useEffect(() => {
    setOpen(isOpen);
  }, [isOpen]);

  const handleClose = () => {
    setOpen(false);
  }

  return (
    <div>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={open}
        onClose={handleClose}
        autoHideDuration={6000}
      >
        <Alert severity="success">
          Checkout Success!
        </Alert>
      </Snackbar>
    </div>
  );
}

export default memo(DisplaySnackbar);