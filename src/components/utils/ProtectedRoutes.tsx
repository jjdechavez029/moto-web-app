import React from "react";
import { Route, Redirect } from "react-router-dom";
import CircularLoading from "./Loading";
import { getAccessToken } from "./accessToken";

export interface User {
    id: number | null;
    firstName: string;
    lastName: string;
    email: string;
}

const ProtectedRoute = ({ 
    component: Component,
    auth: { user, isAuthenticated },
    ...rest
} : any) => {
    const token = getAccessToken(); 

    if (user === null) {
        return <CircularLoading />
    } else {
        return (
            <Route 
                {...rest}
                render={(props) => {
                    if (!token && !isAuthenticated) return <Redirect to={{
                        pathname: '/login',
                        state: {from: props.location}
                    }} />
                    return <Component {...props}  />
                }}       
            />

        )
    }

}

export default ProtectedRoute;