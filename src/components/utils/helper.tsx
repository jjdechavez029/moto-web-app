import React from "react";
import { Cart } from "../../reducers/CartReducer";

export function beautifyPrice(price: number) {
    return (
        <React.Fragment>
            &#8369; {price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
        </React.Fragment>
    )
}

export function totalPrice(arr: Cart[]) {
    let prices = [];
    for(let i of arr) {
        prices.push(+i["totalAmount"] * i["quantity"])
    }
   
    return prices.reduce((x, y) => {
        return x + y
    } ,0);
}

export function getTotalQuantity(arr: Cart[]) {
    let quantity = [];
    for (let i of arr) {
        quantity.push(i["quantity"])
    }

    return quantity.reduce((x, y) => x + y ,0);
}