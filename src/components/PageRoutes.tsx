import React, { useContext } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
// import lazyImport from "./lazyImport";
import Dashboard from "./pages/dashboard";
import Cookie from "./pages/Cookie";
import Login from "./authentication/Login";
import ProtectedProviderRoute from "./utils/ProtectedProviderRoute";
import ProtectedRoute from "./utils/ProtectedRoutes";
import ItemContextProvider from "../contexts/dashboard/ItemContext";
import Checkout from "./pages/checkout";
import Header from "./pages/dashboard/Header";
import { getAccessToken } from "./utils/accessToken";
import { About } from "./pages/dashboard/About";
import { AuthContext } from "../contexts/AuthContext";
import { CartContext } from "../contexts/dashboard/CartContext";

// const Login = lazyImport("./authentication/Login");
// const Dashboard = lazyImport("./dashboard");
// const ProtectedRoute = lazyImport("./utils/ProtectedRoutes");

const PageRoutes = (): JSX.Element => {
  const { authState, dispatch } = useContext(AuthContext);
  const { cartState } = useContext(CartContext);
  const token = getAccessToken();

  return (
    // <Suspense fallback={<div>Loading...</div>}>
      <Router>
        <div>
          {token && authState.user && <Header authDispatch={dispatch} cartState={cartState} />}
          <Switch>
            <Route path="/login" component={Login} />
            <ProtectedProviderRoute exact path="/" provider={ItemContextProvider} component={Dashboard} auth={authState} />
            <ProtectedProviderRoute path="/checkout" provider={ItemContextProvider} component={Checkout} auth={authState} />
            <ProtectedRoute path="/about" component={About} auth={authState} />
            <Route path="/cookie" component={Cookie} />
          </Switch>
        </div>
      </Router>
    // </Suspense>
  );
}

export default PageRoutes;