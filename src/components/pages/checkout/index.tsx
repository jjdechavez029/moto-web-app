import React, { useContext } from "react";
import { 
    Grid, 
    makeStyles, 
    Container, 
} from "@material-ui/core";
import { CartContext } from "../../../contexts/dashboard/CartContext";
import CartList from "./CartList";
import Total from "./Total";
import { ItemContext } from "../../../contexts/dashboard/ItemContext";
import DisplaySnackbar from "../../utils/reusable-component/SnackBar";

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh'
    },
    grid: {
        height: '50vh'
    },
}));

const Checkout = () => {
    const classes = useStyles();
    const { 
        state: { finishedCheckout },
        setState: { setFinishedCheckout, setCheckout },
        cartState: { cart },
        dispatch: cartDispatch
    } = useContext(CartContext);
    const { dispatch: itemDispatch } = useContext(ItemContext);

    return (
        <Container maxWidth="lg" className={classes.root}>
            <Grid container spacing={2} className={classes.grid}>
                <Grid item lg={8} sm={12} xs={12}>
                    <CartList
                        items={cart}
                        cartDispatch={cartDispatch}
                        itemDispatch={itemDispatch}
                    />
                </Grid>
                <Grid item lg={4} sm={12} xs={12} >
                    <Total 
                        items={cart} 
                        cartDispatch={cartDispatch}
                        updateCheckout={setCheckout}
                        handleFinishedCheckout={setFinishedCheckout}
                    />
                </Grid>
            </Grid>
            <DisplaySnackbar isOpen={finishedCheckout} />
        </Container>
    )
}

export default Checkout;