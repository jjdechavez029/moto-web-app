import React from "react";
import { 
    makeStyles,
    List, 
    ListItem, 
    ListItemText, 
    Typography, 
    Paper, 
    Divider,
    ListItemSecondaryAction,
    Button,
    Box,
    IconButton
} from "@material-ui/core";
import { Cart } from "../../../reducers/CartReducer";
import { beautifyPrice } from "../../utils/helper";
import CloseIcon from '@material-ui/icons/Close';

const styles = makeStyles((theme) => ({
    paper: {
        marginTop: '3em',
        height: '200px',
        // marginRight: 'auto',
        // marginLeft: 'auto',
        // width: '750px'
    },
    title: {
        padding: '.25em .75em'
    },
    list: {
        padding: '8px 1.25em'
    },
    quantityBtn: {
        minWidth: '10px'
    },
    secondaryAction: {
        width: '150px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    }
}));

type iProps = {
    items: Cart[];
    cartDispatch({ type, payload } : { type: string, payload?: any | null }): void;
    itemDispatch({ type, payload } : { type: string, payload?: any | null }): void;
}

const CartList = ({
    items,
    cartDispatch,
    itemDispatch
}: iProps) => {
    const classes = styles();

    const removeFromCart = (id: number) => {
        cartDispatch({ type: 'REMOVE_INTO_CART', payload: { id } });
        itemDispatch({ type: 'DOWNGRADE_STATUS_ITEM', payload: id });
    }

    const increaseItemQuantity = (id: number) => {
        cartDispatch({ type: 'INCREASE_CART_ITEM_QUANTITY', payload: id })
        itemDispatch({ type: 'INCREASE_ITEM_QUANTITY', payload: id })
    }

    const decreaseItemQuantity = (id: number) => {
        cartDispatch({ type: 'DECREASE_CART_ITEM_QUANTITY', payload: id })
        itemDispatch({ type: 'DECREASE_ITEM_QUANTITY', payload: id })
    }

    return (
        <Paper component="div" className={classes.paper}>
            <Typography variant="h6" className={classes.title}>
                Item Cart
            </Typography>
            <Divider />
            <List className={classes.list}>
                {items.length > 0 ? 
                    items.map((item: Cart) => (
                        <ListItem key={item.name}>
                            <ListItemText
                                primary={item.name}
                                secondary={beautifyPrice(item.totalAmount)}
                            />
                            <ListItemSecondaryAction className={classes.secondaryAction}>
                                <Button 
                                    variant="outlined" 
                                    onClick={() => decreaseItemQuantity(item.id!)} 
                                    className={classes.quantityBtn}
                                    disabled={item.quantity === 1}
                                >
                                    -
                                </Button>
                                <Box component="span" style={{ color: 'red' }}>{item.quantity}</Box>   
                                <Button 
                                    variant="outlined" 
                                    onClick={() => increaseItemQuantity(item.id!)}
                                    className={classes.quantityBtn}
                                >
                                    +
                                </Button>
                                <IconButton 
                                    onClick={() => removeFromCart(item.id!)}
                                >
                                    <CloseIcon />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    ))
                    : (
                        <ListItem>
                            <ListItemText 
                                secondary="You don't have items on cart" 
                                style={{ textAlign: 'center' }}
                            />
                        </ListItem>
                    )
                }
            </List>
        </Paper>
    );
}

export default CartList;