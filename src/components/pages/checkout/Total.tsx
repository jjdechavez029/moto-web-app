import React, { useState, useEffect } from "react";
import { 
    makeStyles,
    Typography, 
    Paper, 
    Divider,
    Grid,
    Button,
    Container,
} from "@material-ui/core";
import { Cart } from "../../../reducers/CartReducer";
import { totalPrice, beautifyPrice, getTotalQuantity } from "../../utils/helper";
import { checkout } from "../../../actions/CartAction";

const styles = makeStyles((theme) => ({
    paper: {
        marginTop: '3em',
        height: '200px',
        // marginRight: 'auto',
        // marginLeft: 'auto',
        // width: '750px'
    },
    title: {
        padding: '.25em .75em'
    },
    grid: {
        // width: '100%',
        // margin: '2em'
    }
}));

type iProps = {
    items: Cart[];
    cartDispatch({ type, payload } : { type: string, payload: null | string }): void;
    handleFinishedCheckout: React.Dispatch<React.SetStateAction<boolean>>;
    updateCheckout: React.Dispatch<React.SetStateAction<boolean>>;
}


const Total = ({
    items,
    cartDispatch,
    updateCheckout,
    handleFinishedCheckout
}: iProps) => {
    const classes = styles();
    const [ids, setIds] = useState<[number?]>([]);
    const [quantity, setQuantity] = useState<[number?]>([]);

    useEffect(() => {
        let idsArr: [number?]= [];
        let quantityArr: [number?] = [];
        items.map(item => {
            idsArr.push(item.id);
            quantityArr.push(item.quantity)
        });

        setIds(idsArr);
        setQuantity(quantityArr);
    }, [items]);

    const handleCheckout = (
        ids: [number?], 
        quantity: [number?], 
        cartDispatch: any
    ) => {
        checkout(ids, quantity, cartDispatch);
        handleFinishedCheckout(true);
        updateCheckout(false);
    }

    return (
        <Paper component="div" className={classes.paper}>
            <Typography variant="h6" className={classes.title}>
                Total Price
            </Typography>
            <Divider />
            <Container style={{marginTop: '1em'}}>
                <Grid 
                    className={classes.grid} 
                    container 
                    spacing={2} 
                    justify="center" 
                    direction="column"
                >
                    <Grid item md={12} sm={12} xs={12}>
                        <Typography variant="h6" align="center" style={{ fontSize: '1.10rem' }}>
                            {beautifyPrice(totalPrice(items))} 
                        </Typography>
                    </Grid>
                    <Grid item md={12} sm={12} xs={12}>
                        <Typography variant="body2" align="center" style={{ fontSize: '.75rem' }}>
                            Quantity {getTotalQuantity(items)} 
                        </Typography>
                    </Grid>
                    <Grid item md={12} sm={12} xs={12}>
                        <Button 
                            variant="contained" 
                            fullWidth 
                            disabled={items.length === 0}
                            onClick={() => handleCheckout(ids, quantity, cartDispatch)}
                        >
                            Checkout
                        </Button>
                    </Grid>
                </Grid>
            </Container>
        </Paper>
    );
}

export default Total;