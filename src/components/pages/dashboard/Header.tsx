import React, { useState } from "react";
import clsx from 'clsx';
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import InfoIcon from '@material-ui/icons/Info';
import DesktopWindowsIcon from '@material-ui/icons/DesktopWindows';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { LogoutUser } from "../../../actions/AuthActions";
import { CartState } from "../../../reducers/CartReducer";
import { withStyles, Badge, ListSubheader } from "@material-ui/core";

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    appBar: {
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      transition: theme.transitions.create(['margin', 'width'], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
      justifyContent: 'flex-end',
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -drawerWidth,
    },
    contentShift: {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    },
    link: {
      textDecoration: 'none',
      color: 'rgba(0, 0, 0, 0.87)'
    }
  }),
);

const StyledBadge = withStyles((theme) => ({
  badge: {
    border: `2px solid ${theme.palette.background.paper}`,
    padding: '0 4px',
  },
}))(Badge);

type iProps = {
  authDispatch({ type, payload } : { type: string, payload?: any | null}): void;
  cartState: CartState;
}

const Header = ({
  authDispatch,
  cartState
} : iProps) => {
  const classes = useStyles();
  const theme = useTheme();

  const [open, setOpen] = useState(false);
  const { cart } = cartState;

  const [mainLink] = useState([
    { name: 'Inventory', icon: <DesktopWindowsIcon />, to: '/' },
    { name: 'Checkout', icon: <ShoppingCartIcon />, to: '/checkout' }
  ]);
  const [optionLink] = useState([
    { name: 'About', icon: <InfoIcon />, to: '/about' },
    { name: 'Logout', icon: <ExitToAppIcon />, to: '/login' }
  ]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="sticky"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, open && classes.hide)}
          >
            <Badge 
              color="secondary" 
              variant="dot" 
              invisible={cart.length <= 0}
              style={{ padding: '0 0px' }}
            >
              <MenuIcon />
            </Badge>
          </IconButton>
          <Link to="/" style={{ textDecoration: 'none', color: '#fff' }}>
            <Typography variant="h6" noWrap>
              Moto Inventory System
            </Typography>
          </Link>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
          </IconButton>
        </div>
        <Divider />
        <List>
          {mainLink.map((link, index) => (
            <Link 
              key={index} 
              to={link.to} 
              className={classes.link}
              onClick={handleDrawerClose}
            >
              <ListItem button>
                <ListItemIcon>{link.icon}</ListItemIcon>
                <ListItemText primary={link.name} />
                <ListSubheader>
                  {index === 1 && cart.length > 0 && <StyledBadge badgeContent={cart.length} color="secondary" />}
                </ListSubheader>
              </ListItem>
            </Link>
          ))}
        </List>
        <Divider />
        <List>
          {optionLink.map((link, index) => (
            <Link 
              key={index} 
              to={link.to} 
              className={classes.link} 
              onClick={() => index === 1 ? LogoutUser(authDispatch) : handleDrawerClose()}
            >
              <ListItem button>
                <ListItemIcon>{link.icon}</ListItemIcon>
                <ListItemText primary={link.name} />
              </ListItem>
            </Link>
          ))}
        </List>
      </Drawer>

    </div>
  )
}

export default Header;