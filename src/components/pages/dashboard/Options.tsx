import React, { memo } from 'react';
import { Button, Box } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { Items } from '../../../contexts/dashboard/ItemContext';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    box: {
        // marginTop: theme.spacing(2)
    },
    button: {
      margin: theme.spacing(1),
    },
  }),
);

interface iProps {
    checkout: boolean;
    onToggleDialog: (type: string, item?: Items) => void;
    handleCheckout: React.Dispatch<React.SetStateAction<boolean>>;
    checkoutDispatch({ type, payload } : { type: string, payload?: any | null }): void;
    itemDispatch({ type, payload } : { type: string, payload?: any | null }): void;
}

export const Options = memo((props: iProps) => {
    const classes = useStyles();
    const {
        checkout,
        checkoutDispatch,
        itemDispatch,
        handleCheckout
    } = props;

    const toggleCheckout = () => {
        if (checkout) {
            checkoutDispatch({ type: 'CLEAR_CART' });
            itemDispatch({ type: 'RESET_ALL_ITEMS_QUANTITY' });
        }
            
        handleCheckout(!checkout);
    }

    return (
        <Box display="flex" justifyContent="flex-end" className={classes.box}>
            <Button
                variant="contained"
                startIcon={<AddIcon />}
                className={classes.button}
                onClick={() => props.onToggleDialog('create')}
            >
                Create Item
            </Button>
            <Button
                variant="contained"
                style={{ backgroundColor: checkout ? '#e91e63' : '#2196f3', color: '#fff' }}
                startIcon={checkout ? <CloseIcon /> : <ShoppingCartIcon />}
                className={classes.button}
                onClick={toggleCheckout}
            >
                {checkout ? 'Cancel' : 'Checkout'}
            </Button>
        </Box>
    )
});