import React, { useContext } from 'react';
import ItemsComp from './Items';
import { Container } from '@material-ui/core';
import { CartContext } from '../../../contexts/dashboard/CartContext';

const Dashboard = () => {
    const { 
        state: { checkout },
        setState: { setCheckout },
        dispatch
    } = useContext(CartContext);
    return (
        <Container>
            <ItemsComp 
                checkout={checkout}
                updateCheckout={setCheckout}
                cartDispatch={dispatch}
            />
        </Container>
    )
}

export default Dashboard;